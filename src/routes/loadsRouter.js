const router = require('express').Router();

const {
  addLoad,
  getUsersLoadById,
  getUsersLoad,
  updateUsersLoad,
  deleteUsersLoad,
  getActiveLoad,
  postLoadById,
  loadStateTransition,
  getShippingInfo,
} = require('../controllers/loadsController');

const { authMiddleware } = require('../middlewares/authMiddleware');
const { shipperRoleMiddleware } = require('../middlewares/shipperRoleMiddleware');
const { driverRoleMiddleware } = require('../middlewares/driverRoleMiddleware');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

router.post('/', authMiddleware, shipperRoleMiddleware, asyncWrapper(addLoad));

router.post('/:id/post', authMiddleware, shipperRoleMiddleware, asyncWrapper(postLoadById));

router.patch('/active/state', authMiddleware, driverRoleMiddleware, asyncWrapper(loadStateTransition));

router.get('/active', authMiddleware, driverRoleMiddleware, asyncWrapper(getActiveLoad));

router.get('/:id/shipping_info', authMiddleware, shipperRoleMiddleware, asyncWrapper(getShippingInfo));

router.get('/:id', authMiddleware, shipperRoleMiddleware, asyncWrapper(getUsersLoadById));

router.get('/', authMiddleware, asyncWrapper(getUsersLoad));

router.put('/:id', authMiddleware, shipperRoleMiddleware, asyncWrapper(updateUsersLoad));

router.delete('/:id', authMiddleware, shipperRoleMiddleware, asyncWrapper(deleteUsersLoad));

module.exports = {
  loadsRouter: router,
};
