const router = require('express').Router();

const {
  register,
  login,
  forgotPassword,
} = require('../controllers/authController');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

router.post('/register', asyncWrapper(register));

router.post('/login', asyncWrapper(login));

router.post('/forgot_password', asyncWrapper(forgotPassword));

module.exports = {
  authRouter: router,
};
