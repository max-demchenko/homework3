const router = require('express').Router();

const {
  showTrucks,
  addTruck,
  getTruckById,
  updateTruck,
  deleteTruck,
  assignTruck,
} = require('../controllers/truckController');

const { authMiddleware } = require('../middlewares/authMiddleware');
const { driverRoleMiddleware } = require('../middlewares/driverRoleMiddleware');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

router.get('/', authMiddleware, driverRoleMiddleware, asyncWrapper(showTrucks));

router.get('/:id', authMiddleware, driverRoleMiddleware, asyncWrapper(getTruckById));

router.post('/', authMiddleware, driverRoleMiddleware, asyncWrapper(addTruck));

router.put('/:id', authMiddleware, driverRoleMiddleware, asyncWrapper(updateTruck));

router.delete('/:id', authMiddleware, driverRoleMiddleware, asyncWrapper(deleteTruck));

router.post('/:id/assign', authMiddleware, driverRoleMiddleware, asyncWrapper(assignTruck));

module.exports = {
  truckRouter: router,
};
