const router = require('express').Router();

const {
  showUser,
  deleteUser,
  changePassword,
} = require('../controllers/userController');

const { authMiddleware } = require('../middlewares/authMiddleware');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

router.get('/me', authMiddleware, asyncWrapper(showUser));

router.delete('/me', authMiddleware, asyncWrapper(deleteUser));

router.patch('/me/password', authMiddleware, asyncWrapper(changePassword));

module.exports = {
  userRouter: router,
};
