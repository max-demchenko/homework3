const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config();

class MongoManager {
  constructor(config) {
    this.config = config;
  }

  getConnectionURL() {
    return this.config;
  }

  isConnected() {
    mongoose.connection.on('connected', (_) => true);
  }

  connect() {
    return mongoose.connect(
      this.getConnectionURL(),
    );
  }

  disconnect() {
    mongoose.disconnect();
  }
}

module.exports = {
  mongo: new MongoManager(process.env.CONFIG),
};
