const { mongo } = require('../../mongoManager');
const { LoadModel } = require('./load');

class LoadDAO {
  constructor() {
    mongo.connect();
  }

  async createLoad(loadInfo) {
    const load = new LoadModel();
    Object.assign(load, loadInfo);
    return load.save();
  }

  async getUsersLoadById(userId, loadId) {
    return LoadModel.findOne({ created_by: userId, _id: loadId });
  }

  async getUsersLoads(userId) {
    return LoadModel.find({ created_by: userId });
  }

  async getDriversLoads(userId) {
    return LoadModel.find({ assigned_to: userId });
  }

  async updateUsersLoad(userId, loadId, updatedObj) {
    const load = await LoadModel.findOne({ created_by: userId, _id: loadId });

    return load.update(
      {
        name: updatedObj.name,
        payload: updatedObj.payload,
        pickup_address: updatedObj.pickup_address,
        delivery_address: updatedObj.delivery_address,
        dimensions: updatedObj.dimensions,
        logs: [...load.logs, {
          message: 'Load info updated',
          time: new Date().toISOString(),
        }],
      },
      { runValidators: true },
    );
  }

  async updateLoadStatus(loadId, driverId, newStatus, newState) {
    return LoadModel.findOneAndUpdate(
      { _id: loadId },
      { status: newStatus, assigned_to: driverId, state: newState },
      { runValidators: true },
    );
  }

  async loadLog(loadId, newLog) {
    const load = await LoadModel.findOne({ _id: loadId });
    return load.update(
      {
        logs: [...load.logs, newLog],
      },
      { runValidators: true },
    );
  }

  async deleteUsersLoad(userId, loadId) {
    return LoadModel.findByIdAndDelete({ created_by: userId, _id: loadId });
  }

  async getAssignedLoad(userId) {
    return LoadModel.findOne({ assigned_to: userId });
  }

  async getUserIdByLoadId(loadId) {
    return LoadModel.findById(loadId).then((load) => load.assigned_to);
  }
}
const loadDao = new LoadDAO();

module.exports = {
  loadDao,
};
