const mongoose = require('mongoose');

const loadStatuses = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];
const loadStates = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
  'NOTASSIGNED',
];

const loadSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    enum: loadStatuses,
    required: true,
  },
  state: {
    type: String,
    enum: loadStates,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: [
    {
      message: {
        type: String,
        required: true,
      },
      time: {
        type: typeof Date(),
        required: true,
      },
    },
  ],
  created_date: {
    type: typeof Date(),
    required: true,
  },
});

const Load = mongoose.model('load', loadSchema);

module.exports = {
  LoadModel: Load,
};
