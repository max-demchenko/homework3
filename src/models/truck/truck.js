const mongoose = require('mongoose');

const truckStatuses = ['IS', 'OL', 'IDLE'];
const truckTypes = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];

const truckSchema = mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
    enum: truckTypes,
  },
  status: {
    type: String,
    enum: truckStatuses,
    required: true,
  },
  created_date: {
    type: typeof Date(),
    required: true,
  },
});

const Truck = mongoose.model('truck', truckSchema);

module.exports = {
  TruckModel: Truck,
};
