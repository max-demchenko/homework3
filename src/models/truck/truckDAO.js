const { mongo } = require('../../mongoManager');
const { TruckModel } = require('./truck');

class TruckDAO {
  constructor() {
    mongo.connect();
  }

  async createTruck(truckInfo) {
    const truck = new TruckModel();
    Object.assign(truck, truckInfo);
    return truck.save();
  }

  async showTrucks(id) {
    return TruckModel.find({ created_by: id });
  }

  async getTruckById(usersId, truckId) {
    return TruckModel.findOne({ created_by: usersId, _id: truckId });
  }

  async updateTruckById(usersId, truckId, newType) {
    return TruckModel.findOneAndUpdate(
      { created_by: usersId, _id: truckId },
      { type: newType },
      { runValidators: true },
    );
  }

  async deleteTruck(userId, truckId) {
    return TruckModel.deleteOne({ created_by: userId, _id: truckId });
  }

  async assignTruck(userId, truckId) {
    return TruckModel.findOneAndUpdate(
      { _id: truckId },
      { assigned_to: userId, status: 'IS' },
      { runValidators: true },
    );
  }

  async disassignTruck(truckId) {
    return TruckModel.findOneAndUpdate(
      { _id: truckId },
      { assigned_to: ' ', status: 'IS' },
      { runValidators: true },
    );
  }

  async changeTruckStatus(truckId, newStatus) {
    return TruckModel.findOneAndUpdate(
      { _id: truckId },
      { status: newStatus },
      { runValidators: true },
    );
  }

  async searchTruckInService() {
    return TruckModel.find(
      { status: 'IS' },
    );
  }

  async getTruckByDirverId(driverId) {
    return TruckModel.findOne({ assigned_to: driverId });
  }
}
const truckDao = new TruckDAO();

module.exports = {
  truckDao,
};
