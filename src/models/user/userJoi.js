const joi = require('joi');

const registerUserSchema = joi.object({

  email: joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required(),

  role: joi.string()
    .valid('DRIVER', 'SHIPPER')
    .required(),

  password: joi.string()
    .pattern(/^[a-zA-Z0-9]{3,30}$/)
    .required(),

  createdDate: joi.date()
    .iso()
    .required(),

});

const logInUserSchema = joi.object({

  email: joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required(),

  password: joi.string()
    .pattern(/^[a-zA-Z0-9]{3,30}$/)
    .required(),

});

module.exports = {
  registerUserSchema,
  logInUserSchema,
};
