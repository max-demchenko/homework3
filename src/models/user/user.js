const mongoose = require('mongoose');

const validRoles = ['DRIVER', 'SHIPPER'];

const userSchema = mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
    enum: validRoles,
  },
  assignedTruck: {
    type: String,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: typeof Date(),
    required: true,
  },
});

const User = mongoose.model('user', userSchema);

module.exports = {
  UserModel: User,
};
