const { mongo } = require('../../mongoManager');
const { UserModel } = require('./user');

class UserDAO {
  constructor() {
    mongo.connect();
  }

  async createUser(userInfo) {
    const user = new UserModel();
    const existingUser = await this.findUser({ email: userInfo.email }).catch(() => null);
    if (existingUser) {
      throw new Error('This email is already used');
    }
    Object.assign(user, userInfo);
    return user.save();
  }

  async findUser(searchInfo) {
    const user = await UserModel.findOne(searchInfo);
    if (!user) {
      throw new Error('User not found');
    }
    return user;
  }

  async findById(id) {
    const user = await UserModel.findById(id);
    if (!user) {
      throw new Error('User not found');
    }
    return user;
  }

  async deleteById(id) {
    const user = await UserModel.findOneAndDelete(id);
    if (!user) {
      throw new Error('User not found');
    }
    return user;
  }

  async UpdateById(id, newPassword) {
    const user = await UserModel.updateOne({ _id: id }, { password: newPassword });

    if (!user) {
      throw new Error('User not found');
    }
    return user;
  }

  async assignTruck(truckId, userId) {
    return UserModel.findOneAndUpdate(
      { _id: userId },
      { assignedTruck: truckId },
      { runValidators: true },
    );
  }

  async getAssignedTruckId(userId) {
    return UserModel.findById(userId).then((user) => user.assignedTruck);
  }
}
const userDao = new UserDAO();

module.exports = {
  userDao,
};
