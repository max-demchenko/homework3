const driverRoleMiddleware = async (req, res, next) => {
  const { role } = req.user;
  if (role === 'DRIVER') {
    return next();
  }
  return res.status(400).send({ message: 'Only available for DRIVER role' });
};

module.exports = {
  driverRoleMiddleware,
};
