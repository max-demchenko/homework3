const shipperRoleMiddleware = async (req, res, next) => {
  const { role } = req.user;
  if (role === 'SHIPPER') {
    return next();
  }
  return res.status(400).send({ message: 'Only available for SHIPPER role' });
};

module.exports = {
  shipperRoleMiddleware,
};
