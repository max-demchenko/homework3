const jwt = require('jsonwebtoken');

const secretKey = process.env.SECRET_KEY;

const authMiddleware = async (req, res, next) => {
  const token = req.headers.authorization?.split(' ')[1];

  if (!token) {
    return res.status(400).send({ message: 'Not authorized' });
  }

  return jwt.verify(token, secretKey, (err, payload) => {
    if (err) {
      return res.status(400).send({ message: 'Invalid token' });
    }
    req.user = {
      userId: payload.id,
      role: payload.role,
      assignedTruck: payload.assignedTruck,
    };
    return next();
  });
};

module.exports = {
  authMiddleware,
};
