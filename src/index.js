require('dotenv').config();

const PORT = process.env.PORT || 8080;

const express = require('express');
const fs = require('fs');
const morgan = require('morgan');
const path = require('path');

const app = express();
app.use(express.json());

const accessLogStream = fs.createWriteStream(path.basename('access.log'), {
  flags: 'a',
});
app.use(morgan('combined', { stream: accessLogStream }));

const { authRouter } = require('./routes/authRouter');
const { userRouter } = require('./routes/userRouter');
const { truckRouter } = require('./routes/truckRouter');
const { loadsRouter } = require('./routes/loadsRouter');

app.use('/api/auth', authRouter);
app.use('/api/user', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadsRouter);

const errorHandler = (err, req, res, next) => {
  console.log('catch');
  console.error(err.message);
  return res
    .status(400)
    .send({ message: 'Server error', error: err.message || err.name || err });
};

app.use(errorHandler);

app.listen(PORT);
