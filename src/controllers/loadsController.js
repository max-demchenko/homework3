/* eslint-disable camelcase */
const { loadDao } = require('../models/load/loadDao');
const { truckDao } = require('../models/truck/truckDAO');
const { userDao } = require('../models/user/userDAO');

const truckSpecification = {
  SPRINTER: {
    height: 250,
    width: 170,
    length: 300,
    payload: 1700,
  },
  'SMALL STRAIGHT': {
    height: 250,
    width: 170,
    length: 500,
    payload: 2500,
  },
  'LARGE STRAIGHT': {
    height: 350,
    width: 200,
    length: 700,
    payload: 4000,
  },
};

const addLoad = async (req, res, next) => {
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;
  const { userId } = req.user;

  const load = {
    created_by: userId,
    assigned_to: ' ',
    status: 'NEW',
    state: 'NOTASSIGNED',
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    logs: [
      {
        message: 'Load created',
        time: new Date().toISOString(),
      },
    ],
    created_date: new Date().toISOString(),
  };

  return loadDao.createLoad(load).then(() => res.send({ message: 'Load created successfully' }));
};

const getUsersLoadById = async (req, res, next) => {
  const { userId } = req.user;
  const loadId = req.params.id;
  return loadDao.getUsersLoadById(userId, loadId).then((load) => res.send({ load }));
};

const getUsersLoad = async (req, res, next) => {
  const { userId, role } = req.user;
  if (role === 'DRIVER') {
    return loadDao.getDriversLoads(userId).then((loads) => res.send({ loads }));
  }
  return loadDao.getUsersLoads(userId).then((loads) => res.send({ loads }));
};

const updateUsersLoad = async (req, res, next) => {
  const { userId } = req.user;
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;
  const loadId = req.params.id;
  loadDao.updateUsersLoad(userId, loadId, {
    name, payload, pickup_address, delivery_address, dimensions,
  }).then(() => { res.send({ message: 'Load details changed successfully' }); });
};

const deleteUsersLoad = async (req, res, next) => {
  const { userId } = req.user;
  const loadId = req.params.id;
  return loadDao.deleteUsersLoad(userId, loadId).then(() => { res.send({ message: 'Load deleted successfully' }); });
};

const postLoadById = async (req, res, next) => {
  const { userId } = req.user;
  const loadId = req.params.id;
  const load = await loadDao.getUsersLoadById(userId, loadId);
  const trucksInService = await truckDao.searchTruckInService();
  if (load.status !== 'NEW') {
    return res.status(400).send({ message: 'This load is already assigned or shipped' });
  }
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < trucksInService.length; i++) {
    if (
      truckSpecification[trucksInService[i].type].height > load.dimensions.height
      && truckSpecification[trucksInService[i].type].width > load.dimensions.width
      && truckSpecification[trucksInService[i].type].length > load.dimensions.length
      && truckSpecification[trucksInService[i].type].payload > load.payload
    ) {
      loadDao.updateLoadStatus(loadId, trucksInService[i].assigned_to, 'ASSIGNED', 'En route to Pick Up');
      loadDao.loadLog(loadId, {
        message: `Load assigned to driver with id ${trucksInService[i].assigned_to}`,
        time: new Date().toISOString(),
      });
      truckDao.changeTruckStatus(trucksInService[i]._id, 'OL');
      return res.send({ message: 'Load posted successfull', driver_found: true });
    }
  }
  loadDao.loadLog(loadId, {
    message: 'No fitting trucks found',
    time: new Date().toISOString(),
  });
  return res.status(400).send({ message: 'no trucks in service' });
};

const loadStateTransition = async (req, res, next) => {
  const { userId } = req.user;
  const load = await loadDao.getAssignedLoad(userId);
  if (load.state === 'En route to Pick Up') {
    loadDao.loadLog(load._id, {
      message: 'Arrived to Pick Up',
      time: new Date().toISOString(),
    });
    return loadDao.updateLoadStatus(load._id, userId, 'ASSIGNED', 'Arrived to Pick Up').then(() => {
      res.send({ message: 'Load state changed to \'Arrived to Pick Up\'' });
    });
  } if (load.state === 'Arrived to Pick Up') {
    loadDao.loadLog(load._id, {
      message: 'En route to delivery',
      time: new Date().toISOString(),
    });
    return loadDao.updateLoadStatus(load._id, userId, 'ASSIGNED', 'En route to delivery').then(() => {
      res.send({ message: 'Load state changed to \'En route to delivery\'' });
    });
  } if (load.state === 'En route to delivery') {
    loadDao.loadLog(load._id, {
      message: 'Arrived to delivery',
      time: new Date().toISOString(),
    });
    const truckId = await userDao.getAssignedTruckId(userId);
    truckDao.changeTruckStatus(truckId, 'IS');
    return loadDao.updateLoadStatus(load._id, userId, 'SHIPPED', 'Arrived to delivery').then(() => {
      res.send({ message: 'Load state changed to \'Arrived to delivery\'' });
    });
  }
  return res.status(400).send({ message: 'Invalid' });
};

const getActiveLoad = async (req, res, next) => {
  const { userId } = req.user;
  return loadDao.getAssignedLoad(userId).then((load) => { res.send({ load }); });
};

const getShippingInfo = async (req, res, next) => {
  const { userId } = req.user;
  const loadId = req.params.id;
  const load = await loadDao.getUsersLoadById(userId, loadId);
  const driverId = await loadDao.getUserIdByLoadId(loadId);
  const truck = await truckDao.getTruckByDirverId(driverId);

  res.send({ load, truck });
};

module.exports = {
  addLoad,
  getUsersLoadById,
  getUsersLoad,
  updateUsersLoad,
  deleteUsersLoad,
  getActiveLoad,
  postLoadById,
  loadStateTransition,
  getShippingInfo,
};
