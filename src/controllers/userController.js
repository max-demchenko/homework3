const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { userDao } = require('../models/user/userDAO');

const showUser = async (req, res, next) => {
  const { userId } = req.user;
  const user = await userDao.findById(userId);
  const responseBody = {
    _id: userId,
    role: user.role,
    email: user.email,
    created_date: user.createdDate,
  };
  return res.send(responseBody);
};

const deleteUser = async (req, res, next) => {
  const { userId } = req.user;
  return userDao.deleteById(userId).then(() => res.send({ message: 'Profile deleted successfully' }));
};

const changePassword = async (req, res, next) => {
  const { userId } = req.user;
  const { oldPassword, newPassword } = req.body;

  if (!oldPassword || !newPassword) {
    return next('Provide old and new passwor');
  }

  const user = await userDao.findById(userId);
  console.log(user);

  const passwordIsValid = await bcrypt.compare(oldPassword, user.password);

  if (!passwordIsValid) {
    return res.status(400).send({ message: 'Invalid password' });
  }

  const changedPassword = await bcrypt.hash(newPassword, 10);

  if (userDao.UpdateById(userId, changedPassword)) {
    return res.send({
      message: 'Password changed successfully',
    });
  }
  return next('internal error');
};

module.exports = {
  showUser,
  deleteUser,
  changePassword,
};
