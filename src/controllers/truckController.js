const { truckDao } = require('../models/truck/truckDAO');
const { userDao } = require('../models/user/userDAO');

const showTrucks = async (req, res, next) => {
  const { userId } = req.user;
  return truckDao.showTrucks(userId).then((trucks) => res.send({ trucks }));
};

const addTruck = async (req, res, next) => {
  const { type } = req.body;
  const { userId } = req.user;
  const newTruck = {
    created_by: userId,
    assigned_to: ' ',
    type,
    status: 'IS',
    created_date: new Date().toISOString(),
  };

  return truckDao.createTruck(newTruck).then(() => res.send({ message: 'Truck created successfully' }));
};

const getTruckById = async (req, res, next) => {
  const { userId } = req.user;
  const truckId = req.params.id;
  return truckDao.getTruckById(userId, truckId).then((truck) => res.send(truck));
};

const updateTruck = async (req, res, next) => {
  const { userId } = req.user;
  const truckId = req.params.id;
  const newTruckType = req.body.type;
  return truckDao.updateTruckById(userId, truckId, newTruckType)
    .then(() => res.send({ message: 'Truck details changed successfully' }));
};

const deleteTruck = async (req, res, next) => {
  const { userId } = req.user;
  const truckId = req.params.id;
  return truckDao.deleteTruck(userId, truckId).then(() => res.send({ message: 'Truck deleted successfully' }));
};

const assignTruck = async (req, res, next) => {
  try {
    const { userId } = req.user;
    const truckId = req.params.id;

    const assignedTruck = await userDao.findUser({ _id: userId })
      .then((user) => user.assignedTruck);
    if (assignedTruck !== ' ') {
      truckDao.disassignTruck(assignedTruck);
    }
    userDao.assignTruck(truckId, userId);
    truckDao.changeTruckStatus(truckId, 'IS');
    return truckDao.assignTruck(userId, truckId).then(() => res.send({ message: 'Truck assigned successfully' }));
  } catch (err) {
    return next(err);
  }
};
module.exports = {
  showTrucks,
  addTruck,
  getTruckById,
  updateTruck,
  deleteTruck,
  assignTruck,
};
