const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { registerUserSchema, logInUserSchema } = require('../models/user/userJoi');
const { userDao } = require('../models/user/userDAO');

const secretKey = process.env.SECRET_KEY;

const register = async (req, res, next) => {
  const { email, password, role } = req.body;
  const newUser = {
    email,
    password,
    role,
    createdDate: new Date().toISOString(),
  };

  const validationResult = registerUserSchema.validate(newUser)?.error?.message;

  if (validationResult) {
    return next({ name: validationResult });
  }

  newUser.password = await bcrypt.hash(password, 10);

  if (role === 'DRIVER') {
    newUser.assignedTruck = ' ';
  }

  return userDao.createUser(newUser).then(() => res.send({ message: 'Profile created successfully' }));
};

const login = async (req, res, next) => {
  const { email, password } = req.body;
  const validationResult = logInUserSchema.validate(req.body)?.error?.message;

  if (validationResult) {
    return next({ name: validationResult });
  }

  const user = await userDao.findUser({ email });

  const passwordIsValid = await bcrypt.compare(password, user.password);

  if (!passwordIsValid) {
    return res.status(400).send({ message: 'Invalid password' });
  }

  const jwtPayload = {
    email: user.email,
    id: user._id,
    role: user.role,

  };

  if (user.role === 'DRIVER') {
    jwtPayload.assignedTruck = user.assignedTruck;
  }
  const token = await jwt.sign(jwtPayload, secretKey);

  return res.send({ jwt_token: token });
};

const forgotPassword = async (req, res, next) => {
  const { email } = req.body;
  const user = await userDao.findUser({ email });
  if (user?.email) {
    return res.send({ message: 'New password sent to your email address' });
  }
  return next('This user does\'t exist');
};

module.exports = {
  register,
  login,
  forgotPassword,
};
